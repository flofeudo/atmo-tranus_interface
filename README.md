# Tranus-Visum network conversion

Constructing a Tranus-Visum correpondence of networks, to be able to compute Tranus assignement results in the Visum network. The Tranus network is a simplification of the Visum network, with less nodes and edges, mostly suppressing pass-through links. This script creates the "biyection" between the nodes and edges of both networks, permiting to represents the various link-based results from the Tranus network in the Visum original network.

## Getting Started

### Prerequisites
The script is in python language, and needs the following python dependencies: 
* numpy
* pandas 
* networkx.

(just install  anaconda python : https://anaconda.org/anaconda/python and you are good)
	
Various input files are needed to execute the script. These files are located in the [Input_Files](Input_Files) folder. The paths to the input files must be entered in the [input_files.py](input_files.py) file.

* The Visum Network [adj_visum2015.csv](adj_visum2015.csv): 
	You wil need the Visum network in the form of an adjacency list, this is a file containing a list of edges.
	
	```
		1, 2
		2, 1
		3, 5
	```

This adjacency list is constructed from the data from the shapefile of the visum network.

* The Tranus Network:
	This file is constructed based on the Tranus network and the nodes that have a Visum id. To build this network, you will need to supply the Tranus .links file. The supplied file is [NEW-00A.links](NEW-00A.links). You will also need to supply a correspondance file, to identify wich Tranus nodes have a Visum ID. This file can be created using Joint in excel, or map in python, between Tranus .nodes file and the description column. The supplied file is [biyection_tranus_visum.csv](biyection_tranus_visum.csv), this file is just a csv with two columns, establishing the biyection between the Tranus ID/Visum ID of the nodes.
	
	```
		Tranus ID, Visum ID
		1		 , 1		
		2		 , 2		
		3        , 4		
	```

* Assignement File [assignement_fausto.csv](assignement_fausto.csv):
	This file is the assignement output from Tranus. The script will convert the Tranus id of nodes to the nodes with the visum jointure using the biyection file.

### Running the code:
First verify that you have all required files listed in [input_files.py](input_files.py) in the [Input_Files](Input_Files) folder.
To run the program, you will need to execute the script atmo.py using python (you can also do ipython >> run atmo.py).

```
>> python atmo.py 
Biyection read succesfully from file: Input_Files/biyection_tranus_visum.csv
Tranus network read succesfully from file: Input_Files/tranus_visum_attributes.csv
Tranus assisgnement file read succesfully: Input_Files/assignement_fausto.csv
Reading Visum network from: Input_Files/adj_visum2015.csv
Warning: There is (are) 1 node(s) that do not have a visum ID                
  The followgin Tranus nodes do not have a Visum Id:
   3657, 
  Removing nodes {   3657, } from the Tranus graph.   
Choose an Operator: 
          1 : V-Voiture 
          2 : Ve-Velo 
          3 : P-Pietons 
          4 : Poids Lurds
          5 : B-Bus urbain
          6 : C-Car
          7 : N-Navettes
          8 : S-Tramway
          9 : Bplus_BHNS
         10 : Cplus-Car Exp
         11 : T-Train
Please eneter a number between 1 and 11.
Type Operator Number >> 1
  
```

You have to input the number of the desired operator.

```
V-Voiture is selected.
Edges not found in the Tranus Network:
 (31, 108143447)
 (196, 108130136)
 (229, 108129510)
 (341, 108142226)
 (11631, 108123957)
 (11677, 108118545)
 (11754, 108118686)
 (12247, 108125304)
 (13437, 13446)
 (13446, 13437)
 (13607, 108142473)
 (13941, 108125338)
 (108118545, 11677)
 (108118686, 11754)
 (108118984, 108125784)
 (108123258, 108125410)
 (108123957, 11631)
 (108125304, 12247)
 (108125338, 13941)
 (108125410, 108123258)
 (108125784, 108118984)
 (108126285, 108126289)
 (108126289, 108126285)
 (108126289, 108126354)
 (108126354, 108126289)
 (108126517, 108126600)
 (108126600, 108126517)
 (108128369, 108135985)
 (108128549, 108128804)
 (108128745, 108128853)
 (108128787, 108128851)
 (108128804, 108128549)
 (108128851, 108128787)
 (108128851, 108128853)
 (108128853, 108128745)
 (108128853, 108128851)
 (108129510, 229)
 (108129739, 108129858)
 (108129813, 108129694)
 (108129889, 108129973)
 (108129994, 108129889)
 (108130075, 108130198)
 (108130976, 108131010)
 (108132533, 108132549)
 (108132685, 108132720)
 (108132834, 108132799)
 (108135985, 108128052)
 (108136275, 108137314)
 (108137387, 108137390)
 (108137390, 108137387)
 (108142226, 341)
 (108142473, 13607)
 (108143447, 31)
 (108144466, 108144505)
 (108144505, 108144466)
 (108144508, 108144510)
 (108144874, 108144885)
 (108144885, 108144874)
 (109227060, 109227064)
 (109227064, 109227060)
 (109230732, 109230880)
 (109230816, 109230880)
 (109230880, 109230732)
 (109230880, 109230816)
 (109230880, 109230899)
 (109230880, 109231138)
 (109230899, 109230880)
 (109231138, 109230880)
 Output file: proyection_visum_V-Voiture.csv
 ```

 Edges not present in the Tranus network are listed (it is normal, as there are assignements between some nodes without Visum ID). The output file is then written in [proyection_visum.csv](proyection_visum.csv)
 
 The correpondance of paths is also saved at the end, in the file biy_dictionary.csv[]](biy_dicionary.csv).

 You can re-run the program again if you tap "n" or quit if you tap "q". 

## Authors

* **Thomas Capelle** - *Initial work* - [Cape](https://gitlab.inria.fr/tcapelle)

## License

This project is licensed under the MIT License

## Acknowledgments

* Thank you Fausto Lo Feudo
