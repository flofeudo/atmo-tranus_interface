
import os.path

#import libraries
import pandas as pd
import numpy as np
import networkx as nx 

from input_files import *
from heapq import heappush, heappop

#print options
pd.set_option('display.width',1400)

#Reading Input Files
try:
    biy = pd.read_csv(biyection_file, 
                      header=None, 
                      index_col=0, 
                      dtype = int).squeeze()
    print "Biyection read succesfully from file: %s"%biyection_file
except IOError:
    print "  Error: Biyection Tranus - Visum file: %s  not found, \
            maybe wrong fie name?"%biyection_file

if not os.path.isfile(tranus_network_file):
    try:
        tranus_links  = pd.read_csv('NEW-00A.links',  
                                    dtype = {'Origin Id':int, 
                                             'Destination Id':int, 
                                             'Link-Type Id': int, 
                                             'description': str})

    except IOError:
        print "  Error: Tranus Links file: %s  not found, \
                maybe wrong fie name?"%links_file
    else:
        del tranus_links['Link Id']
        del tranus_links['Dir']
        tranus_links.columns = tranus_links.columns.map(str.strip)
        valid_nodes = tranus_links['Origin Id'].isin(biy.index) & \
                      tranus_links['Destination Id'].isin(biy.index)
        tranus_links_v = tranus_links[valid_nodes]

        tranus_links_v['Origin Id'].replace(biy, inplace=True)
        tranus_links_v['Destination Id'].replace(biy, inplace=True)
        tranus_links_v.to_csv('Input_Files/tranus_visum_attributes.csv',index=False, 
                              header=False)
else:
    T = nx.read_edgelist(tranus_network_file, 
                         delimiter=',', 
                         create_using = nx.DiGraph(),
                         data =  (('Link-Type Id', int), 
                                  ('Distance', float), 
                                  ('Capacity', int),
                                  ('name', str), 
                                  ('description', str),
                                 ), 
                         nodetype=int)
    print "Tranus network read succesfully from file: %s"%tranus_network_file

try:
    assignement_df = pd.read_csv(assignement_file, delimiter =';', 
                                  dtype = {'Orig':int, 'Dest':int})

except IOError:
    print "  Error: Tranus assisgnement file: %s not found, \
            maybe wrong fie name?"%assisgnement_file
else:
    del assignement_df['Id']
    valid_rows = assignement_df['Orig'].isin(biy.index) & \
                  assignement_df['Dest'].isin(biy.index)
    assignement_df = assignement_df[valid_rows]
    assignement_df['Orig'].replace(biy, inplace=True)
    assignement_df['Dest'].replace(biy, inplace=True)
    print "Tranus assisgnement file read succesfully: %s"%assignement_file



try:
    print "Reading Visum network from: %s"%visum_network_file
    V = nx.read_adjlist(visum_network_file, delimiter = ",", nodetype=int)    
    #reading the visum graph from adjacency list
    V = V.to_directed()
except IOError:
    print "  Error: reding Visum network, maybe wrong fie name?"

#Compute node difference between graphs:




def graph_difference(V, T, biy):
    '''Computes missing nodes from the Tranus->Visum graphs'''
    d = set.difference(set(T), set(V))

    t_list = []
    aux = []
    for x in d:
        try:
            t_list.append(biy[biy==x].index[0])
        except:
            aux.append(x)
    if len(d)>0:
        print "Warning: There is (are) %s node(s) that do not have a visum ID\
                \n  The followgin Tranus nodes do not have a Visum Id:"%len(d)
        conflicted_nodes = "   "
        for x in t_list:
            conflicted_nodes += str(x) + ", " 
        print conflicted_nodes

        print "  Removing nodes {%s} from the Tranus graph." %conflicted_nodes
        T.remove_nodes_from(d)
    return t_list



def path_search(V, T, source):
    '''path_search(V, T, source):
    Computes all the paths from node source in graph V to the nearest 
    nieghbours in the original Tranus graph T (the ones with Visum ID)
    retuns two dictionaries, the first is a dictionary of nodes and the 
    correponding path to get to them, the second is a dictionary of links
    in graph T and their corresponding path in graph V.
    Ex:
    In: corresponance(V, T, 2)
    Out: 
    ({2: [2, 108131012, 2],
      108130904: [2, 108131012, 108130904],
      108131012: [2, 108131012],
      108131035: [2, 108131035]},
     {(2, 108130904): [2, 108131012, 108130904], (2, 108131035): \
     [2, 108131035]})
    '''

    push = heappush
    pop = heappop
    biy = {}  # dictionary of final biyections
    seen = {}
    paths = {} # dictionary of final paths
    fringe = []

    seen[source] = 0
    push(fringe, (source, source))
    paths[source] = [source]
    while fringe:
        u, t = pop(fringe)
        for v in V[u]:
            paths[v] = paths[u] + [v]
            if v not in seen:
                seen[v] = seen[u] + 1
                if v not in T:
                    push(fringe, (v, t))
                if v in T:
                    biy[(t, v)] = paths[v]
    return paths, biy




def proyection(assignement_df, biy_dictionary):
    #You may change the output variable to select which columns are exported
    #in the output file.
   

    # output = ['LinkCap', Dem/Cap', 'StVeh', 'TotVeh', 'ServLev', 'OperId', 'OperName',
    #             'Capac', 'Demand', 'Vehics', 'Dem/Cap.1', 'StVeh.1', 
    #             'IniSpeed', 'FinSpeed', 'Energy']
    output = ['OperName', 'LinkCap', 'Vehics', 'TotVeh', 'IniSpeed', 'FinSpeed']

    row_list = []
    edges_not_in_visum = []
    print "Edges not found in the Tranus Network:"
    for idx, row in assignement_df.iterrows():
        o, d = row['Orig'], row['Dest']
        if (o,d) in  biy_dictionary:
            visum_path = biy_dictionary[(o,d)]
            path_length = len(visum_path) - 1
            path_item = 1
            for i, j in zip(visum_path[:-1], visum_path[1:]):
                # print row[output].values.tolist()
                if i == o and j == d:
                    new_row = [i, j] + row[output].values.tolist()
                else:
                    new_row = [i, j] + row[output].values.tolist()
                row_list.append(new_row)
                path_item += 1
                # mask_ij = (assignement_df['Orig'] == i) & (assignement_df['Dest'] == j)
        else:
            print " (%s, %s)"%(o,d)
            edges_not_in_visum.append((o,d))
    return pd.DataFrame(row_list, columns = ['Orig', 'Dest'] + output)


#Utility
def mask_od(assignement_df, o, d):
    return assignement_df[(assignement_df['Orig'] == o) & (assignement_df['Dest'] == d)]



##MAIN
graph_difference(V, T, biy)

biy_dictionary = {}     #key (a,d) : path = [a,b,c,d]

for n in T:
    _, biy_n = path_search(V,T,n) #ignore the first output
    biy_dictionary.update(biy_n)

def run_program(assignement_df, biy_dictionary):
    operators = ['V-Voiture', 'Ve-Velo', 'P-Pietons', '"Poids Lurds"', 
                 '"B-Bus urbain"', 'C-Car', 'N-Navettes', 'S-Tramway', 
                 'Bplus_BHNS', '"Cplus-Car Exp"', 'T-Train']
    print 'Choose an Operator: \n\
              1 : V-Voiture \n\
              2 : Ve-Velo \n\
              3 : P-Pietons \n\
              4 : Poids Lurds\n\
              5 : B-Bus urbain\n\
              6 : C-Car\n\
              7 : N-Navettes\n\
              8 : S-Tramway\n\
              9 : Bplus_BHNS\n\
             10 : Cplus-Car Exp\n\
             11 : T-Train\n\
              0 : All'

    choice = -1
    while choice not in range(0, 12):
        print "Please eneter a number between 0 and 11."
        choice = raw_input('Type Operator Number >>  ')
        try:
            choice = int(choice)
        except:
            print " '%s'is not a valid number."%choice
            
    if choice == 0:
        print "All is selected."
        assignement_choice = assignement_df
    else:
        choice = operators[choice - 1]
        print "%s is selected."%choice
        assignement_choice = assignement_df[assignement_df['OperName'] == choice]

    df_out = proyection(assignement_choice, biy_dictionary)
    output_filename = 'proyection_visum_' + str(choice) +'.csv'
    df_out.to_csv(output_filename, index=False)
    print 'Output file: %s'%output_filename



    #Saving the computed graph-biyection to a csv file to visualize in excel
    #the dictionary is converted to string.
    with open("biy_dictionary.csv", "w") as w:
        for key, val in biy_dictionary.items():
            w.write(str(key) +'; ' + str(val)+ '\r\n')
    print "Saving the graph biyection dictionary to biy_dictionary.csv"
    w.close()

run_program(assignement_df, biy_dictionary)
while True:
    choice = raw_input('Enter (n) to run again, (q) to exit >> ')
    if choice == 'n':
        run_program(assignement_df, biy_dictionary)
    else:
        if choice == 'q':
            print '>> Good Bye! <<'
            break
        else:
            print 'Please type: n or q (without the brackets)'